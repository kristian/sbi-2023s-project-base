from flask import Flask, request
from werkzeug.middleware.proxy_fix import ProxyFix

from app.util import sortString

app = Flask(__name__)
app.wsgi_app = ProxyFix( app.wsgi_app, x_for=1, x_host=1)

@app.route("/")
def hello_world():
    return "<p>Hello, World!</p>"


@app.route("/item")
def entrypoint_hero_item_list():
    return {"items": ["Skateboard", "Pizza", "Batmobile", "Cape"] }

@app.route("/item/<item>/hero")
def entrypoint_hero_by_item(item):
    if item in ["Skateboard", "Pizza"]:
        return "Turteles"
    elif item in ["Batmobile", "Cape"]:
        return "Batman"
    else:
        return "Unknown"

@app.route("/item/<item>/statistic")
def entrypoint_item_statistic(item):
    return { "hero": entrypoint_hero_by_item(item),
             "length": len(item),
             "sorted": sortString(item)
           }


@app.post("/jsoninfo")
def entrypoint_post_jsoninfo():
    json = request.form['jsondata']
    return { "length": len(json), "isValid": "Maybe" }


@app.get("/jsoninfo")
def entrypoint_get_jsoninfo():
    return """<html><head><title>jsoninfo</title></head><body><form action="/jsoninfo" method="post"><textarea roes="10" cols="40" name="jsondata"></textarea><br /><input type="submit" /></form></body></html>"""
