import unittest

# Import own tests to run
from tests.util import *
from tests.entrypoints import *

# Help for unittests see: https://docs.python.org/3/library/unittest.html


# Run all tests.
if __name__ == '__main__':
    unittest.main()
