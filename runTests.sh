#!/bin/bash

docker image inspect sbi-project-testing > /dev/null 2>&1 || docker build --tag sbi-project-testing docker
docker container run --rm -v `pwd`/app:/app/app:ro -v `pwd`/data:/app/data -v `pwd`/tests:/app/tests:ro sbi-project-testing python -m unittest tests
